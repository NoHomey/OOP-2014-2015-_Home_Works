#include <iostream>
#include <cstdlib> 
#include <exception>	
using namespace std;

template<class T>class List {
	struct Node {
		T val_;
		Node* prev_;
		Node* next_;
		Node(Node* prev = 0, Node* next = 0)
		: prev_(prev), next_(next)
		{}
		Node(const T& val, Node* prev = 0, Node* next = 0) 
		: val_(val), prev_(prev), next_(next)
		{}
	};
	Node* head_;
	Node* tail_;
	template <typename t> friend  ostream& operator<<(ostream&, const List<t>&);
public:
	List()
	: head_(new Node), tail_(new Node) {
		link();
	}
	List(const T& first, const T& last) 
	: head_(new Node), tail_(new Node) {
		link();
		for(T i = first;i < last;++i)
			push_back(i);
	}
	List(const List& list)
	: head_(new Node), tail_(new Node) {
		link();
		append(list);
	}
	~List() {
		clear();
		delete head_;
		delete tail_;
	}
	void link() {
		head_->next_ = tail_;
		tail_->prev_ = head_;
	}
	void append(const List& list) {
		for(Const_Iterator it = list.begin();it != list.end();++it) {
			push_back(*it);
		}
	}
	void push_back(const T& val) {
		insert(end(), val);
	}
	void except() const{
		if(empty())
			throw exception();
	}
	void pop_back() {
		erase(last());
	}
	void push_front(const T& val) {
		insert(begin(), val);
	}
	void pop_front() {
		erase(begin());
	}
	T& front() {
		return *begin();
	}
	const T& front() const{
		return front();
	}
	T& back() {
		return tail_->prev_->val_;
	}
	const T& back() const{
		return back();
	}
	bool empty() const{
		return (head_->next_ == tail_);
	}
	size_t size() const{
		size_t i = 0;
		for(Const_Iterator it = begin();it != end();++it) {
			i++;
		}
		return i;
	}
	void clear() {
		while(!empty())
			pop_back();
	}
	void swap(List& list) {
		List tmp = list;
		list = *this;
		*this = tmp;
	}
	List& operator=(const List& list) {
		if(this != &list) {
			clear();
			append(list);
		}
		return *this;
	}
	class Iterator {
	protected: Node* pos_;
	public:
			Iterator(Node* pos = 0) 
			: pos_(pos)
			{}
			Iterator operator++() {
				pos_ = pos_->next_;
				return *this;
			}
			Iterator operator++(int) {
				Iterator res = *this;
				operator++();
				return res;
			}
			bool operator==(const Iterator& it) const{
				return (pos_ == it.pos_);
			}
			bool operator!=(const Iterator& it) const{
				return !operator==(it);
			}
			T& operator*() {
				return pos_->val_;
			}
			T* operator->() {
				return &operator*();
			}
			Node* node() {
				return pos_;
			}
	};
	class Const_Iterator: public Iterator {
	public:
		Const_Iterator(Node* pos = 0)
		: Iterator(pos)
		{}
		const T& operator*() {
			return Iterator::operator*();
		}
		const T* operator->() {
			return &operator*();
		}
	};
	class Reverse_Iterator: public Iterator {
	public:
			Reverse_Iterator(Node* pos = 0)
			: Iterator(pos)
			{}
			Reverse_Iterator operator++() {
				Iterator::pos_ = Iterator::pos_->prev_;
				return *this;
			}
			Reverse_Iterator operator++(int) {
				Reverse_Iterator res = *this;
				operator++();
				return res;
			}
	};
	class Const_Reverse_Iterator: public Reverse_Iterator, public Const_Iterator {
	public:
		Const_Reverse_Iterator(Node* pos = 0)
		: Reverse_Iterator(pos)
		{}
		using Reverse_Iterator::operator++;
		using Const_Iterator::operator*;
		using Const_Iterator::operator->;
	};
	Iterator last() {
		return Iterator(tail_->prev_);
	}
	Iterator begin() {
		return Iterator(head_->next_);
	}
	Const_Iterator begin() const{
		return Const_Iterator(head_->next_);
	}
	Iterator end() {
		return Iterator(tail_);
	}
	Const_Iterator end() const{
		return Const_Iterator(tail_);
	}
	Reverse_Iterator rbegin() {
		return Reverse_Iterator(tail_->prev_);
	}
	Const_Reverse_Iterator rbegin() const{
		return Const_Reverse_Iterator(tail_->prev_);
	}
	Reverse_Iterator rend() {
		return Reverse_Iterator(head_);
	}
	Const_Reverse_Iterator rend() const{
		return Const_Reverse_Iterator(head_);
	}
	Iterator insert(Iterator pos, const T& val) {
		Node* n = pos.node();
		Node* tmp = new Node(val, n->prev_, n);
		n->prev_->next_ = tmp;
		n->prev_ = tmp;
		return Iterator(tmp);
	}
	Iterator erase(Iterator pos) {
		except();
		Node* n = pos.node();
		Node* tmp = n->next_;
		n->prev_->next_ = tmp;
		tmp->prev_ = n->prev_;
		delete n;
		return Iterator(tmp);
	}
	Iterator erase(Iterator first, Iterator last) {
		except();
		Node* f = first.node();
		Node* l = last.node();
		Node* d;
		f->prev_->next_ = l;
		l->prev_ = f->prev_;
		for(;first != last;first++) {
			d = first.node();
			delete d;
		}
		return Iterator(l);
	}
};

template<class t> ostream& operator<<(ostream& out, const List<t>& list) {
	out << '{';
	for(typename List<t>::Const_Iterator it = list.begin();it != list.end();++it)
		out << *it << ',';
	out << '}' << endl;
	return out;
}

int main (int argc, char** argv) {
	List<int> l1(atoi(argv[1]), atoi(argv[2]));
	List<int> l2(atoi(argv[3]), atoi(argv[4]));
	cout << "l1: " << l1 << "l2: " << l2;	
	size_t c = 0;
	for(List<int>::Iterator it1 = l1.begin();it1 != l1.end();++it1) {
		for(List<int>::Iterator it2 = l2.begin();it2 != l2.end();++it2) {
			if(*it1 == *it2) {
				c++;
				break;
			}
		}
	}
	cout << "equal element in l1 and l2: " << c << endl;
	l1.push_back(-100);
	l2.push_back(-100);
	cout << "l1: " << l1 << "l2: " << l2;	
	List<int> l(l2);
	cout << "l: " << l;
	for(List<int>::Reverse_Iterator r = l1.rbegin();r != l1.rend();++r)
		l.insert(l.begin(), *r);
	cout << "l: " << l;
	List<int>::Iterator bit;
	for(bit = l.begin();bit != l.end();++bit) {
		if(*bit == -100)
			break;
	}
	l.erase(bit, l.end());
	cout << "l: " << l;
	return 0;
}